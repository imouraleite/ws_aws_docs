# Principal

## Criando alertas de Billing
Evitar que haja cobranças na conta desnecessárias na AWS
1) Digite no campo de pesquisa: billing
2) No menu lateral, clique em "budgets"
3) Preencha os dados para seu alerta
4) click em criar orçamento

## CLI e CloudShell
CLI - Command line interface
CloudShell - CLI que está na cloud

## Arquitetura global AWS
### Regioes
AWS implementa seus serviços baseado em regiões espalhadas em diversos lugares do mundo (https://aws.amazon.com/pt/about-aws/global-infrastructure/regions_az/). Um exemplo de região é o Brasil, ele fica na regiao da america do sul (SA). Quando desejamos publicar algum serviço, precisamos seguir o nome da região para isso. Exemplo: sa-east-1

### Zonas de Disponibilidades (AZ)
Uma zona de disponibilidade (AZ) é um ou mais datacenters distintos com energia, rede e conectividade redundantes em uma região da AWS. As Azs são nomeadas de acordo com a região. Exemplo: us-east-1a.

### Zonas Locais
São pequenos data centers mais proximos dos usuarios finais e suas AZs. As zonas locais da AWS aproximam a computação, o armazenamento, o banco de dados e outros produtos da AWS selecionados dos usuários finais. Com as zonas locais da AWS, você pode executar facilmente aplicativos altamente exigentes que exigem latências em milissegundos de um dígito para seus usuários finais, como criação de conteúdo de mídia e entretenimento, jogos em tempo real, simulações de reservatórios, automação de projetos eletrônicos e machine learning.

### AWS Wavelength
O AWS Wavelength permite que os desenvolvedores criem aplicações com latências de um dígito para dispositivos móveis e usuários finais. Wavelength incorporam serviços de computação e armazenamento da AWS aos datacenters dos provedores de telecomunicações na borda das redes 5G e acessam facilmente a variedade de serviços da AWS na região. Isso permite que forneçam aplicativos que exigem latências inferiores a 10 milissegundos.

### AWS Outposts
O AWS Outposts leva produtos, infraestrutura e modelos operacionais nativos da AWS a praticamente qualquer datacenter. Você pode usar as mesmas APIs, ferramentas e infraestrutura da AWS no local e na Nuvem AWS para oferecer uma experiência híbrida verdadeiramente consistente.