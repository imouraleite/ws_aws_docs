# EC2

## Introdução
EC2 significa um Elastic Computer Cloud, um hardware na nuvem que aumenta de acordo com a necessidade.

## Vantagens do EC2
1) Controle completo da Instancia
2) Segurança (Keys para acessar a maquina virtual)
3) Instancias protegidas por firewall
3) Compatibilidade com todos os serviços da AWS
4) Custo baixo de manutenção
5) Descomplicação de operação

## Usando o EC2

### Criando uma instancia
1) No campo de pesquisa, digite EC2 e selecione o serviço
2) No menu lateral clique em Instancias
3) Clique no botão de executar instancia
4) Da-se um  nome da instancia, escolhe uma opção de imagem de SO, especifica o tipo de instancia
5) Cria-se um novo par de chave para fazer acesso
6) Realiza as configurações de rede para acesso a instancia

### Acessando uma instancia
1) No campo de pesquisa, digite EC2 e selecione o serviço
2) No menu lateral clique em Instancias
3) Selecione sua instancia e clique em conectar
4) Se sua instancia é linux, siga ate a aba Cliente SSH e siga os passos para fazer conexao via terminal

### Gerenciado o estado da instancia
1) No campo de pesquisa, digite EC2 e selecione o serviço
2) No menu lateral clique em Instancias
3) Selecione a(s) instancia(s) e clique no botão Estado da Instancia
3.1) Interromper Instancia (Deixa a instancia parada - stop de instancia)
3.2) Reiniciar Instancia (restart)
3.3) Encerrar Instancia (Desliga a instancia e exclui)

### User Data
Parte da configuração da instancia EC2 que vai executar quando a instancia iniciar pela primeira vez.

### Criando uma Instancia com User Data (Dados do Usuario)
1) Siga os passos em Criando uma instancia
2) E antes de criar a instancia vá em Detalhes Avançados
3) Siga até o campo de Dados de Usuario e inclua o script de criação

### Acessando um S3 com uma instancia EC2
1) No campo de pesquisa, digite IAM e selecione o serviço
2) No menu lateral clique em Funções e selecione a opção de Serviço AWS
3) Marque o serviço EC2 e clique em proximo
4) Marque as permissoes que deseja dar e clique proximo
5) De-um nome e crie a função
6) Com a função criada, siga ate o EC2, selecione a instancia no checkbox
7) Clique em Ações > Segurança > Modificar função do IAM
8) Escolher a função criada e clicar no botão Atualizar Funçao do IAM

### Amazon Elastic Container Service (ECS)
O Amazon ECS facilita a implantação, o gerenciamento e a escalabilidade de contêineres do Docker que executam aplicações, serviços e processos em lote. O Amazon ECS coloca contêineres em todo o cluster com base nas necessidades dos recursos e é integrado a recursos conhecidos, como o Elastic Load Balancing, grupos de segurança do EC2, volumes do EBS e perfis do IAM.

./data/ECS_diagram.png

Registry = Imagens Docker, gerenciado pelo Elastic Container Registry - ECR
Task = Docker Container
Service - ECS Service - Gerencia os containers
Task Definition = 
Container Definition = 

### Criando Amazon Elastic Container Service - ECS
1) No campo de pesquisa, digite ECS e selecione o serviço
2) No menu lateral clique em Clusters (Primeiro cria-se um cluster)
2.1) No momento deste curso tinha um botão Comece a usar que facilita a criação do cluster e as tasks
2.2) Clica-se no botao Criar Cluster
3) Escolhemos o tipo de cluster (Escolho o Somente Redes - Funciona com fargate e servless), clique em proximo
4) De um nome ao cluster e por fim clique em create
5) Para adicionar as tasks, no menu lateral clique em Definições de Tarefas
6) Clicar em Criar Nova Definição da Tarefa
7) Escolha as opções de tipo (Fargate)
7.1) O AWS Fargate é um mecanismo de computação sem servidor e com pagamento conforme o uso que permite a você se concentrar em construir aplicações sem gerenciar servidores. O AWS Fargate é compatível com o Amazon Elastic Container Service (ECS) e com o Amazon Elastic Kubernetes Service (EKS).
8) Escolher as configurações do tamanho da task
9) Cria-se um container no botão Adicionar Container
10) Por fim, clica-se em criar
11) Entre no cluster e vá ate a aba task
12) clique em Executar nova Tarefa
13) Selecione a opção do Fargate, SO linux
14) Configure a VPC
15) Clique em Executar Tarefa
