# Introdução a IAM

## Identity and Access Management (IAM)
Docs: https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html?icmpid=docs_iam_console

### Metodos de acesso a conta AWS
Podemos acessar a conta da AWS através dos seguintes meios:
1) Console AWS
2) CLI
3) API

Uma vez realizado o acesso, estamos dentro do IAM. Atraves do serviço do IAM podemos criar usuarios, regras, grupos, federated users e aplications; também podemos gerir as politicas atraves do IBP ou RBP. Uma vez criado as politicas, é possível ter acesso aos serviços.

## Gerenciamento do IAM

### Criação de Usuario
1) Digite no campo de pesquisa IAM e entre no serviço
2) No menu lateral clique em usuário
3) Clique em adicionar usuario
4) Forneça um nome ao usuario
5) Selecione o tipo de acesso
6) clique no botão next
7) Adicione as permissões necessarios para o usuario
8) clique no botão next
9) Adicione Tags (Identificadores) ao usuario
10) clique no botão next
11) Clique em criar usuario

### Adicionando novas policitas a usuarios
1) Digite no campo de pesquisa IAM e entre no serviço
2) No menu lateral clique em usuário
3) Selecione o usuario que deseja adicionar permissoes
4) Clicar em adicionar permissoes
4.1) Adicione as permissões (AdministratorAcess = acesso root)
5) Depois clique em next e confirme as permissoes

### Entendendo sobre o processo de autenticação
Para fazer acesso a AWS via CLI ou API, a AWS solicita um Access Key ID e uma Secret Access Key

### MFA - Multi Fact Aut3ntication
Existem dois tipos de MFA: virtual (app), Fisica (Token).

1) Habilitando o MFA
a) Vá até o usuario e clique na aba Credenciais de Segurança
b) Vá até a seção: Autenticação multifator (MFA) e Clique em Atribuir Dispositivo MFA
c) Dê um nome de identificação e Escolha a opção de dispositivo virtual MFA * Isto irá fazer uso de apps como o google authenticator
d) Escaneie o codigo QR code e adicione os dois codigo que o app fornece

### Alterando Politicas de Senha
Configurando pré-requisitos minimos para a senha

1) Digite no campo de pesquisa IAM e entre no serviço
2) No menu lateral clique em Configurações de Conta
3) Vá na sessão Politicas de Senha e clique em Editar
4) Escolha a politica de senha Personalizado
5) Configure as politicas que desejar e clique em salvar alterações
